# Clickjacking Test

This code is an example about how to test if an application is vulnerable to Clickjacking.

## How to use

Just add the app url into iframe tag on line 8.

### Expected Result

When the application url is added to iframe is expected to not show anything and throw an error in console.
If content is visualized or error is not thrown, it means that our application is vulnerable to Clickjacking. 
